﻿abubaApp.constant("HOST_URI", "http://scccc.vps.phps.kr:8000");
abubaApp.config(["$stateProvider", "$urlRouterProvider", "userProvider", "$ionicConfigProvider", function($stateProvider, $urlRouterProvider, userProvider, $ionicConfigProvider){

	$ionicConfigProvider.tabs.position("bottom");


	/*
	userProvider.set({
		email : "gkljl@klja.com",
		id : "1",
		mem_type : 0,
		mem_type_str : null,
		name : "루라라",
		phone : "0101030",
		user_id : "test"
	});
	*/
	var u = userProvider.load() || {};

        $stateProvider
	.state("common", {
		url:"/common",
		abstract : true,
		templateUrl : "page/common.html"
	})
	.state("common.login", {
                url:"/login",
		templateUrl :"page/login/main.html",
		controller : "loginCtrl" 
	})
        .state("common.terms", {
                url:"/terms",
		templateUrl :"page/terms/main.html",
		controller : "termsCtrl" 
        })  
        .state("common.signUp", {
                url:"/signUp",
		templateUrl :"page/signUp/main.html",
		controller : "signUpCtrl" 
        })  
        .state("common.selectRole", {
                url:"/selectRole",
		templateUrl :"page/selectRole/main.html",
		controller : "selectRoleCtrl" 
        })  
	.state("parent", {
		url:"/parent",
		abstract : true,
		templateUrl : "page/parent/tabs.html"
	})
        .state("parent.child", {
                url:"/child",
		views:{
			"child-tab" : {
				templateUrl :"page/parent/child/home/main.html",
				controller : "parentChildHomeCtrl" 
			}
		},
        })  
        .state("parent.addChild", {
                url:"/addChild",
		views:{
			"child-tab" : {
				templateUrl :"page/parent/child/addChild/main.html",
				controller : "parentAddChildCtrl" 
			}
		},
        })  
        .state("parent.preschool", {
                url:"/preschool",
		views:{
			"preschool-tab" : {
				templateUrl :"page/parent/preschool/home/main.html",
				controller : "parentPreschoolHomeCtrl" 
			}
		},
        })  
        .state("parent.preschoolDetail", {
                url:"/preschoolDetail",
		views:{
			"preschool-tab" : {
				templateUrl :"page/parent/preschool/preschoolDetail/main.html",
				controller : "parentPreschoolDetailCtrl" 
			}
		},
		params:{
			preschool : null
		}
        })  
        .state("parent.pref", {
                url:"/pref",
		views:{
			"pref-tab" : {
				templateUrl :"page/parent/pref/home/main.html",
				controller : "parentPrefHomeCtrl" 
			}
		},
        })  
	.state("directorSide", {
		url:"/directorSide",
		abstract : true,
		templateUrl:"page/director/side.html" 
	})
	.state("director", {
		url:"/director",
		abstract : true,
		templateUrl:"page/director/common.html" 
	})
        .state("directorSide.home", {
                url:"/home",
		views:{
			"director" : {
				templateUrl :"page/director/home/main.html",
				controller : "directorHomeCtrl" 
			}
		}
        })  
        $urlRouterProvider.otherwise("/");

	if(u.mem_type == 0){
		$stateProvider.state("/", {
			url:"/",
			templateUrl :"page/selectRole/main.html",
			controller : "selectRoleCtrl" 
		});
	}else if(u.mem_type == 1){
		$urlRouterProvider.otherwise("/parent/child");
	}else if(u.mem_type == 3){
		$urlRouterProvider.otherwise("/directorSide/home");
	
	}else{
		$stateProvider.state("/", {
			url:"/",
			templateUrl :"page/login/main.html",
			controller : "loginCtrl" 
		});
	}

}]);

abubaApp.run(["$ionicPlatform", "user",  "$state", "$timeout", "$cordovaSplashscreen", "$ionicPlatform", function($ionicPlatform, user, $state, $timeout, $cordovaSplashscreen, $ionicPlatform){
	$ionicPlatform.ready(function(){

	});

}]);

abubaApp.directive('hideTabs', function($rootScope) {
	return {
		restrict: 'A',
		link: function($scope, $el) {
			$rootScope.hideTabs = 'tabs-item-hide';
			/*
			$scope.$on("$ionicView.beforeEnter", function(){
				$rootScope.hideTabs = 'tabs-item-hide';
			});
			*/
			$scope.$on('$ionicView.beforeLeave', function() {
				$rootScope.hideTabs = '';
			});
		}
	};
});
