abubaApp.provider("preschoolModel", ["storageProvider", function(sp){
	var preschoolList = []
	var key = "preschools";


	function save (){
		sp.set(key, preschoolList);
	}
	function load (){
		preschoolList = sp.get(key);
	}
	load();

	return {
		save : save,
		load : load,
		$get:["ajax", "user", "$rootScope", function(ajax, user, $rootScope){
			
			$rootScope.$on("parent.updatePreschool", function(){
				init();
			});
			function init(){
				return ajax.req({
					method:"get",
					url:"/parents/"+user.get().id+"/preschools",
				}).then(function(data){
					preschoolList = data.data;
					save();
				});
			}

			return {
				init : init,
			        get : function(){
					return preschoolList;
				}

			}
		}]
	}
	
}]);
