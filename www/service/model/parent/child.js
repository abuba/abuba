abubaApp.provider("childModel", ["storageProvider", function(sp){
	var childList = []
	var key = "children";


	function save (){
		sp.set(key, childList);
	}
	function load (){
		childList = sp.get(key);
	}

	/*
	 * preshcool_id
	 * preschool_name
	 * name,
	 * birth,
	 * gender,
	 * photo,
	 * parent_id,
	 * class_name,
	 * class_id
	 * id
	 */


	load();



	return {
		save : save,
		load : load,
		$get:["ajax", "user", "$rootScope", function(ajax, user, $rootScope){

			return {
				init : function(userId){
					return ajax.req({
						method:"get",
						url:"/parents/"+userId+"/children",
					}).then(function(data){
						childList = data.data;
						save();
					});
				},
				get : function(){
					return childList;
				},
				add : function(data){
					var u = user.get();
					return ajax.req({
						method:"POST",
						url:"/children/"+u.id,
						data : data 
					}).then(function(data){
						childList.push(data.data);
						save();
						$rootScope.$broadcast("parent.updatePreschool");
						return data;
					});
				}

			}
		}]
	}
	
}]);
