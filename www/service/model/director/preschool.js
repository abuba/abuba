abubaApp.provider("directorPreschoolModel", ["storageProvider", function(sp){
	var preschoolList = []
	var key = "director.preschool";


	function save (){
		sp.set(key, preschoolList);
	}
	function load (){
		preschoolList = sp.get(key);
	}


	load();



	return {
		save : save,
		load : load,
		$get:["ajax", "user", "$rootScope", function(ajax, user, $rootScope){

			return {
				init : function(userId){
					return ajax.req({
						method:"get",
						url:"/directors/"+userId+"/preschools",
					}).then(function(data){
						preschoolList = data.data;
						save();
					});
				},
				get : function(){
					return preschoolList;
				},
				add : function(data){
					var u = user.get();
					return ajax.req({
						method:"POST",
						url:"/preschools/"+u.id,
						data : data
					}).then(function(data){
						preschoolList.push(data.data);
						save();
						return data;
					});
				}

			}
		}]
	}
	
}]);
