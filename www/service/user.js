abubaApp.provider("user", ["storageProvider", function(sp){
	var userInfo=undefined;
	var key= "user";
	userInfo = sp.get(key);



	function set (userObj){
		userInfo = userObj;
		sp.set(key, userInfo);
	}
	function get(){
		return userInfo;
	}


	return {
		set : set, 
		load : function(){
			userInfo = sp.get(key);
			return userInfo;
		},
		$get:["$state", function($state){

			return {
				set : set, 
				get : get,
				logout : function(){
					window.localStorage.clear();
					$state.go("common.login");

				}
			}
		}]
	}
	
}]);
