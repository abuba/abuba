abubaApp.service("ajax", ["$http", "HOST_URI", "$ionicLoading", function($http, HOST_URI, $ionicLoading){



	this.req = function(httpArg){
		if(!httpArg.hideLoading){
			$ionicLoading.show({
				delay:800
			});
		}
		httpArg.url = HOST_URI+httpArg.url;
		return $http(httpArg).then(function(data){
			if(!httpArg.hideLoading){
				$ionicLoading.hide();
			}
			return data.data;
		}, function(err){
			if(!httpArg.hideLoading){
				$ionicLoading.hide();
			}
			return err;
		});
	}
}]);
