abubaApp.provider("storage", [function(){
	var userInfo=undefined;



	function set(key, value){
		localStorage.setItem(key, JSON.stringify(value));
	}
	function get(key){
		return JSON.parse(localStorage.getItem(key));
	}
	return {
		set : set,
		get : get,
		$get:[function(){

			return {
				set : set,
				get : get,
				clear : function(){
					localStorage.clear();
				}
			}
		}]
	}
	
}]);
