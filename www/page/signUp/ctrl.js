﻿abubaApp.controller("signUpCtrl", ["$scope", "ajax", "$cordovaPreferences",  "user", "$state", "$timeout", function($scope, ajax, $cordovaPreferences, user, $state, $timeout){
	$scope.newUser= {
	};
	$scope.errMsg = {
	};
	var userIdReg = /^[A-za-z0-9]{4,15}$/;
	var passwordReg= /^.*(?=.{4,20})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;
	var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var formValid = true;
	$scope.isValid = function(){
		formValid = true;
		var newUser = $scope.newUser;
		if(!newUser.user_id){
			$scope.errMsg.user_id = "아이디를 입력하세요.";
			formValid = formValid && false;
		}else if(!userIdReg.test(newUser.user_id)){
			formValid = formValid && false;
			$scope.errMsg.user_id = "영문 소/대문자, 숫자만을 이용해 4~15자리로 입력하세요.";
		}

		if(!newUser.password){
			$scope.errMsg.password= "비밀번호를 입력하세요.";
			formValid = formValid && false;
		}else if(!passwordReg.test(newUser.password)){
			formValid = formValid && false;
			$scope.errMsg.password = "영문, 숫자만을 조합해 4~20자리로 입력하세요.";
		}


		if(!newUser.password_c){
			formValid = formValid && false;
			$scope.errMsg.password_c = "비밀번호를 다시 입력하세요.";
		}else if(newUser.password != newUser.password_c
			&& (newUser.password != undefined && newUser.password_c != undefined)){
			formValid = formValid && false;
			$scope.errMsg.password_c = "비밀번호와 일치하지 않습니다.";
		}
		if(!newUser.name){
			formValid = formValid && false;
			$scope.errMsg.name = "이름을 입력하세요.";
		}

		if(!newUser.email){
			formValid = formValid && false;
			$scope.errMsg.email = "이메일을 입력하세요.";
		}else if(!emailReg.test(newUser.email)){
			formValid = formValid && false;
			$scope.errMsg.email = "이메일 형식에 맞지 않습니다.";
		}

		if(!newUser.phone){
			formValid = formValid && false;
			$scope.errMsg.phone= "전화번호를 입력하세요.";
		}
		
		return formValid;

	}

	$scope.signUp = function(){
		if($scope.isValid()){
			ajax.req({
				method:"POST",
				url : "/users",
				data : $scope.newUser
			}).then(function(data){
				user.set(data.data);
				$state.go("common.selectRole");

			}, function(err){
			});
		}

	}
	var chkTimer = undefined;
	$scope.chkId = function(){
		if(chkTimer != undefined){
			$timeout.cancel(chkTimer);
			chkTimer=undefined;
		}
		$scope.errMsg.user_id = "";
		if(userIdReg.test($scope.newUser.user_id)){
			chkTimer = $timeout(function(){
				console.log($scope.newUser.user_id);
				ajax.req({
					method:"GET",
					url : "/users/exist/"+$scope.newUser.user_id
				}).then(function(data){
					if(data.result == "success"){
						formValid = !data.exist;
						if(data.exist){
							$scope.errMsg.user_id = "이미 가입된 아이디입니다.";
						}
					}
					console.log(data);
				}, function(err){
				});
			}, 300);
		}
	}
	$scope.isSignUp = function(form){
		return form.$invalid || $scope.newUser.password != $scope.newUser.password_c;

	}
}]);
