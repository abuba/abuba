﻿abubaApp.controller("loginCtrl", ["$scope", "ajax", "user", "$state", "childModel", "preschoolModel",  "$q", "$ionicHistory", "directorPreschoolModel", function($scope, ajax, user, $state, childModel, preschoolModel, $q, $ionicHistory, directorPreschoolModel){


	$scope.user = {};
	$scope.errMsg = undefined;
	$scope.u  = undefined;

	$scope.$on("$ionicView.enter", function(){
		$ionicHistory.clearHistory();
	});

	$scope.login = function(){
		ajax.req({
			method:"POST",
			url : "/login",
			data:$scope.user
		}).then(function(data){
			if(data.result == "success"){
				$scope.errMsg = undefined;
				user.set(data.data);
				$scope.u = user.get();
				if(!$scope.u.mem_type != 0){
					$state.go("selectRole");
				}else if($scope.u.mem_type == 1){	//부모
					parentInit($scope.u.id);
				
				}else if($scope.u.mem_type == 2){	//교사
				}else if($scope.u.mem_type == 3){	//원장
					directorInit($scope.u.id);
				}
				

			}else if(data.result == "fail" && data.error.code == 401){
				$scope.errMsg = "로그인 정보가 없습니다.";
			}

		}, function(data){
			console.log(data);
		});
	}



	function parentInit(parentId){
		$q.all([childModel.init(parentId), preschoolModel.init(parentId)])
		.then(function(val){
			$state.go("parent.child");
		});
		/*
		childModel.init(parentId).then(function(data){
		});
		*/

	}
	function directorInit(directorId){
		$q.all([directorPreschoolModel.init(directorId)])
		.then(function(val){
			$state.go("directorSide.home");
		});
		/*
		childModel.init(parentId).then(function(data){
		});
		*/

	}
}]);
