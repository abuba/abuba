﻿abubaApp.controller("parentAddChildCtrl", ["$scope", "$ionicHistory", "ajax", "$ionicModal", "user", "$ionicPopup", "$timeout", "$state", "childModel", function($scope, $ionicHistory, ajax, $ionicModal, user, $ionicPopup, $timeout, $state, childModel){


	$scope.u = user.get();
	$scope.preschoolList = [];
	$scope.search={
		keyword : ""
	}
	$scope.newChild = {
		photo : "img/user_placeholder.png",
		gender : "M"
	};
	/*
	$scope.$on("$ionicView.enter", function(){
	});
	*/

	$ionicModal.fromTemplateUrl("searchPreschoolModal.html", {
		scope : $scope,
		adnimation : "slide-in-up" 

	}).then(function(modal){
		$scope.searchPreschoolModal = modal;
	});

	$scope.searchPreschool = function(){
		$scope.searchPreschoolModal.show();

	};
	$scope.closeModal = function(){
		$scope.searchPreschoolModal.hide();
	};
	$scope.$on("$destory", function(){
		$scope.searchPreschoolModal.remove();
	});



	$scope.search = function(){
		if($scope.search.keyword){
			ajax.req({
				method:"GET",
				url : "/preschools?keyword="+$scope.search.keyword
			}).then(function(data){
				$scope.preschoolList = data.data;
				console.log($scope.preschoolList );
			}, function (err){
			});
		}else{
			$ionicPopup.alert({
				title :"유치원 이름을 입력해주세요." 
			});


		}
	}
	$scope.selPreschool = function(ps){
		$scope.newChild.preschoolName = ps.preschool_name;
		$scope.searchPreschoolModal.hide();
		ajax.req({
			url:"/preschools/"+ps.preschool_id+"/classes",
			method:"GET"
		}).then(function(data){
			$scope.classList = data.data;
			console.log($scope.classList);

			$timeout(function(){
				$scope.newChild.class_id = $scope.classList[0].id;
			}, 0);

		}, function(err){
			console.log(err);
		});
	}
	$scope.addChild = function(){
		 $scope.newChild.birth = $scope.newChild.birthView.format("yyyy-MM-dd");
		 var newData = angular.copy($scope.newChild);
		 delete newData.birthView;

		 childModel.add( newData)
		 .then(function(data){
			 $state.go("parent.child");
		 }).catch(function(err){
			 console.log(err);
		 });
	}

}]);
