abubaApp.controller("parentPreschoolHomeCtrl", ["$scope", "user", "$ionicHistory", "ajax", "$ionicModal", "preschoolModel", function($scope, user, $ionicHistory, ajax, $ionicModal, preschoolModel){




	$scope.$on("$ionicView.enter", function(){
		$ionicHistory.clearHistory();
		$scope.u = user.get();
		$scope.preschool={
			list : preschoolModel.get()
		}
	});

	$scope.refresh = function(){
		preschoolModel.init().then(function(){
			$scope.preschool.list = preschoolModel.get();
			$scope.$broadcast("scroll.refreshComplete");

		});
	}




}]);
