abubaApp.controller("parentPrefHomeCtrl", ["$scope", "user", "$ionicHistory", "storage", "$state", function($scope, user, $ionicHistory, storage, $state){
	$scope.$on("$ionicView.enter", function(){
		$ionicHistory.clearHistory();
		$scope.u = user.get();
	});
	$scope.logout = function(){
		storage.clear();
		$state.go("common.login");
	}

}]);
