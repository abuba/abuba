abubaApp.controller("selectRoleCtrl", ["$scope", "user", "$state", "$ionicHistory", "ajax", function($scope, user, $state, $ionicHistory, ajax){

	$scope.$on("$ionicView.enter", function(){
		$scope.u = user.get();
		$ionicHistory.clearHistory();
	});

	$scope.parent  = function(){
		ajax.req({
			method:"PUT",
			url:"/users/"+$scope.u.id+"/type",
			data : {
				mem_type : 1
			}
		}).then(function(data){
			user.set(data.data);
			$state.go("parent.child");
		}, function(err){

		});
	}
}]);
