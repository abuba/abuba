abubaApp.controller("directorSideCtrl", ["$scope",  "$ionicSideMenuDelegate", "$state", "user", function($scope, $ionicSideMenuDelegate, $state, user){
	$scope.toggleLeft = function(){
		$ionicSideMenuDelegate.toggleLeft();
	}
	$scope.logout = function(){
		user.logout();

	}
}]);

