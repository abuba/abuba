abubaApp.controller("directorHomeCtrl", ["$scope", "user",  "$ionicHistory", "$ionicSideMenuDelegate", "directorPreschoolModel", function($scope, user, $ionicHistory, $ionicSideMenuDelegate, directorPreschoolModel){
	$scope.preschool={};
	$scope.$on("$ionicView.enter", function(){
		$scope.preschool.list= directorPreschoolModel.get();
		console.log($scope.preschool.list);
		$ionicHistory.clearHistory();
	});
	$scope.toggleLeft = function(){
		$ionicSideMenuDelegate.toggleLeft();
	}
}]);

